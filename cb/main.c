#include <stdio.h>
#include <stdlib.h>
#include "../AT.h"

int main()
{
    printf("Hello world!\n");

    at_cmd("AT+H#",sizeof("AT+H#"));
    at_cmd("AT+PROJECT#",sizeof("AT+PROJECT#"));
    at_cmd("AT+TEST=N01",sizeof("AT+TEST=N01"));
    at_cmd("AT+TEST=N02",sizeof("AT+TEST=N02"));
    at_cmd("AT+TEST=N03",sizeof("AT+TEST=N03"));
    at_cmd("AT+TEST=error",sizeof("AT+TEST=error"));

    return 0;
}
