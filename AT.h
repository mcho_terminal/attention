﻿/*
 * at.h
 *
 *  Created on: 2016年2月18日
 *
 */

#ifndef AT_H_
#define AT_H_

#define __X_AT_OK__ 5
#define __X_CMD_SIZE_ 256
#define __X_CMD_PARAM_COUNT_  5
#define AT_VERSION   3
#define AT_INFO_OUTPUT 0

typedef struct at_cmd {
	const char * name;
	int (*func)(int, char **argv);
	const char * doc;
} at_cmd_t;

typedef struct {
	union {
		unsigned char byte;
		struct {
			unsigned char isSendSta :1;
			unsigned char isSetMac :1;
			unsigned char res :6;
		} bit;
	} flag;
	char macString[13];
} AT_CONFIG;

extern int at_cmd(char *data, unsigned int datalen);
#if AT_INFO_OUTPUT
extern void at_info_send_cb(char* data, uint16_t len );
#endif // AT_INFO_OUTPUT

#endif /* AT_H_ */
