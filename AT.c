/*
 * at.c
 *
 *  Created on: 20180816
 *      Author: mcho
 */

#include "AT.h"
#include "stdio.h"
#include "stdlib.h"
#include "string.h"

#ifndef NULL
#define NULL 0
#endif // NULL

static int process_at_test(int argc, char **argv);
static int process_at_project(int argc, char **argv);
static int process_at_help(int argc, char **argv);

const at_cmd_t at_table[] = { //
                { "AT+H#", process_at_help, "AT+H#\tget at cmd information\r\n" }, //
				{ "AT+TEST=",process_at_test, "AT+TEST=\t test at cmd input by =1,2\r\n"},
				{ "AT+PROJECT#", process_at_project, "AT+PROJECT#\tget project info\r\n"},
				{ NULL, NULL, NULL}//
		};


int at_cmd(char *data, unsigned int datalen) {
	int ret = 0;
	unsigned int cmd_length = 0;
	int index = 0;
	int argc = 0;
	char *chr = NULL, *strPtr = NULL;
	char argv[__X_CMD_PARAM_COUNT_ ][__X_CMD_SIZE_ ];
	char *argvPtr[__X_CMD_PARAM_COUNT_ ];
	for ( ; at_table[index].name != NULL; index++)
	{
		cmd_length = strlen(at_table[index].name);
		if (cmd_length <= datalen)
		{
			if (strncmp((char*) data, at_table[index].name, cmd_length) == 0)
			{
			    #if AT_INFO_OUTPUT
                    at_info_send_cb(help, strlen(help));
                #else
                    printf("\r\n---find cmd %s \r\n", at_table[index].name);
                #endif // AT_INFO_OUTPUT
				argc = 0;
				if(at_table[index].func)
				{
					memset(argv[argc], 0, __X_CMD_SIZE_);
					memcpy(argv[argc], at_table[index].name, cmd_length);
					argvPtr[argc] = argv[argc];
					argc++;
					if ((chr = strchr((char*) data, '=')) != NULL )
					{
						strPtr = strtok(chr + 1, ",");
						if (strPtr) {
							cmd_length = strlen(strPtr);
							cmd_length = (cmd_length >= __X_CMD_SIZE_) ? __X_CMD_SIZE_ : cmd_length;
							memset(argv[argc], 0, __X_CMD_SIZE_);
							memcpy(argv[argc], strPtr, cmd_length);
							argvPtr[argc] = argv[argc];
							argc++;

							while ((strPtr = strtok(NULL, ",")) != NULL ) {
								cmd_length = strlen(strPtr);
								cmd_length = (cmd_length >= __X_CMD_SIZE_) ? __X_CMD_SIZE_ : cmd_length;
								memset(argv[argc], 0, __X_CMD_SIZE_);
								memcpy(argv[argc], strPtr, cmd_length);
								argvPtr[argc] = argv[argc];
								argc++;
								if (argc == 5)
									break;
							}
						}
					}
					ret = at_table[index].func(argc, argvPtr);
				}
				ret = __X_AT_OK__;
				break;
			}
		}
	}

	return ret;
}

static int process_at_help(int argc, char **argv) {
	int index = 0;
	char *help = "\r\n\r\n------------xlink AT help---------\r\n\r\n";

#if AT_INFO_OUTPUT
    at_info_send_cb(help, strlen(help));
#else
    printf("%s",help);
#endif // AT_INFO_OUTPUT
	for (; at_table[index].name != NULL ; index++) {
    #if AT_INFO_OUTPUT
        at_info_send_cb(help, strlen(help));
    #else
		printf( "%s",at_table[index].doc );
    #endif // AT_INFO_OUTPUT
	}
	help = "\r\n\r\n------------xlink AT help END---------\r\n\r\n";
#if AT_INFO_OUTPUT
    at_info_send_cb(help, strlen(help));
#else
    printf("%s",help);
#endif // AT_INFO_OUTPUT

	return __X_AT_OK__;
}

char *test_buf1 = "you are handsome!";
char *test_buf2 = "you are sunshine!";
char *test_buf3 = "you are helpful!";

static int process_at_test(int argc, char **argv){

	if(memcmp((argv[1]),"N01",3)==NULL){
		/*!
		*/
    #if AT_INFO_OUTPUT
        at_info_send_cb(test_buf1,sizeof(test_buf1));
    #else
		printf("%s",test_buf1);
    #endif // AT_INFO_OUTPUT
	}
	else if(memcmp((argv[1]),"N02",3)==NULL){
		/*!
		*/
    #if AT_INFO_OUTPUT
        at_info_send_cb(test_buf2,sizeof(test_buf2));
    #else
		printf("%s",test_buf2);
    #endif // AT_INFO_OUTPUT
	}
	else if(memcmp((argv)[1],"N03",3)==NULL){
		/*!
		*/
	#if AT_INFO_OUTPUT
        at_info_send_cb(test_buf3,sizeof(test_buf3));
    #else
		printf("%s",test_buf3);
    #endif // AT_INFO_OUTPUT
	}
	else {
    #if AT_INFO_OUTPUT
        at_info_send_cb("at test para fail", sizeof("at test para fail"));
    #else
		printf("at test para fail");
    #endif // AT_INFO_OUTPUT

	}

	return __X_AT_OK__;
}

static int process_at_project(int argc, char **argv){

#if AT_INFO_OUTPUT
    uint16_t len = 0;
    char buf[32] = {0};
    len = sprintf(buf,"+OK=%s,%s\r\n","at",__DATE__);
    at_info_send_cb(buf,len);
#else
    printf("+OK=%s,%s\r\n","at",__DATE__);
#endif // AT_INFO_OUTPUT

	return __X_AT_OK__;
}

